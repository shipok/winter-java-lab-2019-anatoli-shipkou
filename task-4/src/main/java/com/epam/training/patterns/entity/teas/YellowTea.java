package com.epam.training.patterns.entity.teas;

import com.epam.training.patterns.entity.Tea;

public class YellowTea extends Tea {
    public YellowTea() {
        description = "Yellow Tea";
    }

    @Override
    public double cost() {
        return 1.09;
    }
}