package com.epam.training.patterns.entity.coffee.adding;

import com.epam.training.patterns.entity.Coffee;
import com.epam.training.patterns.entity.coffee.CoffeeCondimentDecorator;

public class Soy extends CoffeeCondimentDecorator {
    public Soy(Coffee coffee) {
        super(coffee);
    }

    @Override
    public String getDescription() {
        return coffee.getDescription() + ", Soy";
    }

    @Override
    public double cost() {
        return .15 + coffee.cost();
    }
}