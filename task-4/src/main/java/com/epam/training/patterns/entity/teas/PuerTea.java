package com.epam.training.patterns.entity.teas;

import com.epam.training.patterns.entity.Tea;

public class PuerTea extends Tea {
    public PuerTea() {
        description = "Puer Tea";
    }

    @Override
    public double cost() {
        return 1.27;
    }
}