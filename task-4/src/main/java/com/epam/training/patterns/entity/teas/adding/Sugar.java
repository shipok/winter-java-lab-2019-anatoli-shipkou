package com.epam.training.patterns.entity.teas.adding;

import com.epam.training.patterns.entity.Tea;
import com.epam.training.patterns.entity.teas.TeaCondimentDecorator;

public class Sugar extends TeaCondimentDecorator {
    public Sugar(Tea tea) {
        super(tea);
    }

    @Override
    public String getDescription() {
        return tea.getDescription() + ", Sugar";
    }

    @Override
    public double cost() {
        return .09 + tea.cost();
    }
}