package com.epam.training.patterns.entity;

import com.epam.training.patterns.entity.coffee.adding.Chocolate;
import com.epam.training.patterns.entity.coffee.adding.Milk;
import com.epam.training.patterns.entity.coffee.adding.Soy;
import com.epam.training.patterns.entity.coffee.adding.WhippedCream;
import com.epam.training.patterns.extra.InputCheck;

public abstract class Coffee extends Beverage {
    protected boolean isMilk;
    protected boolean isChocolate;
    protected boolean isSoy;
    protected boolean isWhippedCream;

    @Override
    public final Beverage addCondiments(Beverage beverage) {
        if(isMilk) beverage = new Milk((Coffee) beverage);
        if(isChocolate) beverage = new Chocolate((Coffee) beverage);
        if(isSoy) beverage = new Soy((Coffee) beverage);
        if(isWhippedCream) beverage = new WhippedCream((Coffee) beverage);
        return beverage;
    }

    public final int change() {
        System.out.println("1 - Milk/Without\n2 - Chocolate/Without\n3 - Soy/Without\n4 - WhippedCream/Without" +
                "\n5 - set quantity\n0 - back");
        int i = InputCheck.get();
        switch(i) {
            case 1:
                isMilk = !isMilk;
                break;
            case 2:
                isChocolate = !isChocolate;
                break;
            case 3:
                isSoy = !isSoy;
                break;
            case 4:
                isWhippedCream = !isWhippedCream;
                break;
            case 5:
                int num = 0;
                while (num <= 1) {
                    System.out.println("Enter quantity");
                    num = InputCheck.get();
                }
                quantity = num;
                break;
            case 0:
                break;
            default:
                System.err.println("incorrect value");
                break;
        }
        return i;
    }

    public boolean isMilk() {
        return isMilk;
    }

    public boolean isChocolate() {
        return isChocolate;
    }

    public boolean isSoy() {
        return isSoy;
    }

    public boolean isWhippedCream() {
        return isWhippedCream;
    }

    @Override
    public String toString() {
        return description + (isMilk ? ", Milk" : "") + (isChocolate ? ", Chocolate" : "") + (isSoy ? ", Soy" : "") +
                (isWhippedCream ? ", WhippedCream" : "") + "\nquantity = " + quantity;
    }
}