package com.epam.training.patterns.app;

import java.io.File;
import java.io.IOException;

public class Cafe {
    final static File FILE = new File("conclusion/receipt.txt");

    public static void main(String[] args) throws IOException {
        if(FILE.exists()) {
            FILE.delete();
        }

        new MenuMethods().start();
    }
}