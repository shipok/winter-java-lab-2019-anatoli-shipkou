package com.epam.training.patterns.entity.teas;

import com.epam.training.patterns.entity.Tea;

public class OolongTea extends Tea {
    public OolongTea() {
        description = "Oolong Tea";
    }

    @Override
    public double cost() {
        return 1.86;
    }
}