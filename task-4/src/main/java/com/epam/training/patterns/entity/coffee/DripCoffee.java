package com.epam.training.patterns.entity.coffee;

import com.epam.training.patterns.entity.Coffee;

public class DripCoffee extends Coffee {
    public DripCoffee() {
        description = "Drip Coffee";
    }

    @Override
    public double cost() {
        return .88;
    }
}