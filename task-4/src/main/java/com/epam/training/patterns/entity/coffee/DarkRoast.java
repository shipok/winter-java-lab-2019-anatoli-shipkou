package com.epam.training.patterns.entity.coffee;

import com.epam.training.patterns.entity.Coffee;

public class DarkRoast extends Coffee {
    public DarkRoast() {
        description = "Dark Roast";
    }

    @Override
    public double cost() {
        return .99;
    }
}