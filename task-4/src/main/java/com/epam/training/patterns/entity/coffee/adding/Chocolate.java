package com.epam.training.patterns.entity.coffee.adding;

import com.epam.training.patterns.entity.Coffee;
import com.epam.training.patterns.entity.coffee.CoffeeCondimentDecorator;

public class Chocolate extends CoffeeCondimentDecorator {
    public Chocolate(Coffee coffee) {
        super(coffee);
    }

    @Override
    public String getDescription() {
        return coffee.getDescription() + ", Chocolate";
    }

    @Override
    public double cost() {
        return .20 + coffee.cost();
    }
}