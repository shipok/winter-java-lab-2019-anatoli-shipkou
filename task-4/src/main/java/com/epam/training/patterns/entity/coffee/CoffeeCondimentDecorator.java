package com.epam.training.patterns.entity.coffee;

import com.epam.training.patterns.entity.Coffee;

public abstract class CoffeeCondimentDecorator extends Coffee {
    protected Coffee coffee;

    public CoffeeCondimentDecorator(Coffee coffee) {
        this.coffee = coffee;

        isMilk = coffee.isMilk();
        isChocolate = coffee.isChocolate();
        isSoy = coffee.isSoy();
        isWhippedCream = coffee.isWhippedCream();
        quantity = coffee.getQuantity();
    }
}