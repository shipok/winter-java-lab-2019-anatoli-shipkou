package com.epam.training.patterns.util;

import com.epam.training.patterns.entity.Beverage;
import com.epam.training.patterns.entity.coffee.*;
import com.epam.training.patterns.entity.teas.*;

import java.util.LinkedList;
import java.util.List;

public class MenuUtil {
    private static MenuUtil uniqueInstance;

    private MenuUtil() {}

    public static MenuUtil getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new MenuUtil();
        }
        return uniqueInstance;
    }

    public List<Beverage> createMenu() {
        List<Beverage> orders = new LinkedList<>();
        Beverage beverage = new Espresso();
        orders.add(beverage);
        beverage = new HouseBlend();
        orders.add(beverage);
        beverage = new DarkRoast();
        orders.add(beverage);
        beverage = new Decaf();
        orders.add(beverage);
        beverage = new Americano();
        orders.add(beverage);
        beverage = new Cappuccino();
        orders.add(beverage);
        beverage = new DripCoffee();
        orders.add(beverage);
        beverage = new Latte();
        orders.add(beverage);
        beverage = new Mocha();
        orders.add(beverage);

        beverage = new BlackTea();
        orders.add(beverage);
        beverage = new GreenTea();
        orders.add(beverage);
        beverage = new OolongTea();
        orders.add(beverage);
        beverage = new PuerTea();
        orders.add(beverage);
        beverage = new WhiteTea();
        orders.add(beverage);
        beverage = new YellowTea();
        orders.add(beverage);

        return orders;
    }
}