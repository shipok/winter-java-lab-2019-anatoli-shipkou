package com.epam.training.patterns.entity;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private String name;
    private String secondName;
    private String birthday;
    private String phone;
    private String tastePref;
    private List<Beverage> orders = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTastePref() {
        return tastePref;
    }

    public void setTastePref(String tastePref) {
        this.tastePref = tastePref;
    }

    public List<Beverage> getOrders() {
        return orders;
    }

    public void addOrder(Beverage order) {
        orders.add(order);
    }

    public void setOrders(List<Beverage> orders) {
        this.orders = orders;
    }
}