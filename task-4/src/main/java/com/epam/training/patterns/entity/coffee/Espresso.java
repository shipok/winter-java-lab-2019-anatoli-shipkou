package com.epam.training.patterns.entity.coffee;

import com.epam.training.patterns.entity.Coffee;

public class Espresso extends Coffee {
    public Espresso() {
        description = "Espresso";
    }

    @Override
    public double cost() {
        return 1.99;
    }
}