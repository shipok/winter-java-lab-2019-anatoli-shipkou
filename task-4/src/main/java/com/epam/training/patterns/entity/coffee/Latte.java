package com.epam.training.patterns.entity.coffee;

import com.epam.training.patterns.entity.Coffee;

public class Latte extends Coffee {
    public Latte() {
        description = "Latte";
    }

    @Override
    public double cost() {
        return 1.40;
    }
}