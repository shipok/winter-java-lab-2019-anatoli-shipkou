package com.epam.training.patterns.app;

import com.epam.training.patterns.entity.Beverage;
import com.epam.training.patterns.entity.Person;
import com.epam.training.patterns.extra.InputCheck;
import com.epam.training.patterns.util.MenuUtil;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.String.format;

public class MenuMethods {
    public void start() throws IOException{
        List<Person> persons = new ArrayList<>();
        Person person;

        int i = -1;
        while (i != 0) {
            System.out.println("1 - Create user\n2 - Select user\n0 - Exit");
            i = InputCheck.get();
            if(i == 1) {
                persons.add(createPerson());
                System.out.println("  ++  User is created  ++\n");
            } else if(i == 2) {
                while (i != 0) {
                    person = selectPerson(persons);
                    if (person.getName() != null) {
                        orders(person);
                    } else {
                        i = 0;
                    }
                }
                i = -1;
            } else if(i == 0){
                System.out.println(":)");
            } else {
                System.err.println("Invalid input");
            }
        }
    }

    public Person createPerson () {
        Scanner scanner = new Scanner(System.in);
        Person person = new Person();
        System.out.println("Enter name");
        person.setName(scanner.nextLine());
        System.out.println("Enter second name");
        person.setSecondName(scanner.nextLine());
        System.out.println("Enter your birthday");
        person.setBirthday(scanner.nextLine());
        System.out.println("Enter your phone");
        person.setPhone(scanner.nextLine());
        System.out.println("Your taste preferences?");
        person.setTastePref(scanner.nextLine());
        return person;
    }

    public Person selectPerson(List<Person> persons) {
        Person person = new Person();

        if(!persons.isEmpty()) {
            System.out.println("Select a user from the list\n0 - Back");
            int index = 1;
            for (Person p : persons) {
                System.out.println(index++ + " - " + p.getName() + " " + p.getSecondName());
            }

            int i = -1;
            while (i != 0) {
                i = InputCheck.get();
                if(i != 0) {
                    try {
                        person = persons.get(i - 1);
                        i = 0;
                    } catch (IndexOutOfBoundsException e) {
                        System.err.println("Invalid input");
                    }
                }
            }
        } else {
            System.out.println("No users, use '0' - to return");
            out();
        }
        return person;
    }

    public void orders(Person person) throws IOException{
        List<Beverage> orders = MenuUtil.getInstance().createMenu();
        System.out.println("You: " + person.getName() + " " + person.getSecondName());

        int i = -1;
        while (i != 0) {
            System.out.println("1 - Create order\n2 - Show orders\n3 - Cancel order" +
                    "\n4 - Get INVOICE\n0 - back");
            i = InputCheck.get();
            if (i == 1) {
                person.addOrder(createOrder(orders));
            } else if(i == 2) {
                listOrders(person.getOrders());
            } else if(i == 3) {
                cancelOrders(person.getOrders());
            } else if(i == 4) {
                sumOrder(person);
            }
        }
    }

    public Beverage createOrder(List<Beverage> orders) {
        Beverage order = null;

        if(!orders.isEmpty()) {
            System.out.println("Select an item from the list:\n0 - to return");
            int index = 1;
            for (Beverage o : orders) {
                System.out.println(index++ + " - " + o.getDescription());
            }

            int i = -1;
            while (i != 0) {
                i = InputCheck.get();
                if(i != 0) {
                    try {
                        order = orders.get(i - 1);
                        System.out.println("Order is accepted");
                        System.out.println("Complete the order?\n0 - Leave as standard\n1 - Supplement");
                        i = - 1;
                        while (i != 0) {
                            i = InputCheck.get();
                            if(i == 1) {
                                orderChanges(order);
                                i = 0;
                            } else if(i != 0) {
                                System.err.println("Invalid input");
                            }
                        }
                    } catch (IndexOutOfBoundsException e) {
                        System.err.println("Invalid input");
                    }
                }
            }
        } else {
            System.out.println("There is no menu for technical reasons '0' - to return");
            out();
        }
        return order;
    }

    public void listOrders(List<Beverage> orders) {
        if(!orders.isEmpty()) {
            System.out.println("Select an order to view and change");
            int index = 1;
            for (Beverage o : orders) {
                System.out.println(index++ + " - " + o.getDescription());
            }
            System.out.println("0 - to return");

            int i = -1;
            while (i != 0) {
                i = InputCheck.get();
                if(i != 0) {
                    try {
                        Beverage order = orders.get(i - 1);
                        orderChanges(order);
                        orders.set(i-1, order);
                        i = 0;
                    } catch (IndexOutOfBoundsException e) {
                        System.err.println("Invalid input");
                    }
                }
            }
        } else {
            System.out.println("You haven't ordered anything yet, use '0' - to return");
            out();
        }
    }

    public void cancelOrders(List<Beverage> orders) {
        if(!orders.isEmpty()) {
            System.out.println("Select the order you want to cancel");
            int index = 1;
            for (Beverage o : orders) {
                System.out.println(index++ + " - " + o.getDescription());
            }
            System.out.println("0 - to return");

            int i = -1;
            while (i != 0) {
                i = InputCheck.get();
                if(i != 0) {
                    try {
                        orders.remove(i - 1);
                        System.out.println("Order deleted");
                        i = 0;
                    } catch (IndexOutOfBoundsException e) {
                        System.err.println("Invalid input");
                    }
                }
            }
        } else {
            System.out.println("You haven't ordered anything yet, use '0' - to return");
            out();
        }
    }

    public void orderChanges(Beverage order) {
        int i = -1;
        while (i != 0) {
            System.out.println("Position Composition: " + order);
            i = order.change();
        }
    }

    public void sumOrder(Person person) throws IOException {
        List<Beverage> orders = person.getOrders();
        if(!orders.isEmpty()) {
            String receipt = person.getName() + " " + person.getSecondName() + "\nYour RECEIPT:";
            double price = 0.00;

            for(Beverage o : orders) {
                o = o.addCondiments(o);
                price += (o.cost() * o.getQuantity());
                receipt = receipt + "\n\n" + o.getDescription() + "\n_____________\n" + format("%.2f", o.cost()) +
                        " x " + o.getQuantity() + " = " + format("%.2f", o.cost() * o.getQuantity());
            }

            receipt = receipt + "\n============\nTotal: " + format("%.2f", price);

            FileWriter fileWriter = new FileWriter("conclusion/receipt.txt");
            fileWriter.write(receipt);
            fileWriter.close();

            System.out.println(receipt);

            System.exit(0);
        } else {
            System.out.println("You haven't ordered anything yet, use '0' - to return");
            out();
        }
    }

    public void out() {
        int i = -1;
        while (i != 0) {
            i = InputCheck.get();
            if(i != 0) {
                System.err.println("Invalid input");
            }
        }
    }
}