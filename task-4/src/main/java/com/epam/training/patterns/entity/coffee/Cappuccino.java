package com.epam.training.patterns.entity.coffee;

import com.epam.training.patterns.entity.Coffee;

public class Cappuccino extends Coffee {
    public Cappuccino() {
        description = "Cappuccino";
    }

    @Override
    public double cost() {
        return 1.69;
    }
}