package com.epam.training.patterns.entity.coffee;

import com.epam.training.patterns.entity.Coffee;

public class Mocha extends Coffee {
    public Mocha() {
        description = "Mocha";
    }

    @Override
    public double cost() {
        return 1.75;
    }
}