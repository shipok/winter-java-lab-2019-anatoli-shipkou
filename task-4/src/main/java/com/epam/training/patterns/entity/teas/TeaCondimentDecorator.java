package com.epam.training.patterns.entity.teas;

import com.epam.training.patterns.entity.Tea;

public abstract class TeaCondimentDecorator extends Tea {
    protected Tea tea;

    public TeaCondimentDecorator(Tea tea) {
        this.tea = tea;

        isSugar = tea.isSugar();
        isLemon = tea.isLemon();
        isHoney = tea.isHoney();
        quantity = tea.getQuantity();
    }
}