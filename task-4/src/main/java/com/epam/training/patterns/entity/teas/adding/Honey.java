package com.epam.training.patterns.entity.teas.adding;

import com.epam.training.patterns.entity.Tea;
import com.epam.training.patterns.entity.teas.TeaCondimentDecorator;

public class Honey extends TeaCondimentDecorator {
    public Honey(Tea tea) {
        super(tea);
    }

    @Override
    public String getDescription() {
        return tea.getDescription() + ", Honey";
    }

    @Override
    public double cost() {
        return .25 + tea.cost();
    }
}