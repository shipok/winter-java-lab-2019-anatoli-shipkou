package com.epam.training.patterns.entity.teas.adding;

import com.epam.training.patterns.entity.Tea;
import com.epam.training.patterns.entity.teas.TeaCondimentDecorator;

public class Lemon extends TeaCondimentDecorator {
    public Lemon(Tea tea) {
        super(tea);
    }

    @Override
    public String getDescription() {
        return tea.getDescription() + ", Lemon";
    }

    @Override
    public double cost() {
        return .10 + tea.cost();
    }
}