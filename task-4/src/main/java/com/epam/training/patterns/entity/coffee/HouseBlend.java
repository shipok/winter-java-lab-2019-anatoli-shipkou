package com.epam.training.patterns.entity.coffee;

import com.epam.training.patterns.entity.Coffee;

public class HouseBlend extends Coffee {
    public HouseBlend() {
        description = "House Blend Coffee";
    }

    @Override
    public double cost() {
        return .89;
    }
}