package com.epam.training.patterns.entity;

import com.epam.training.patterns.entity.teas.adding.Honey;
import com.epam.training.patterns.entity.teas.adding.Lemon;
import com.epam.training.patterns.entity.teas.adding.Sugar;
import com.epam.training.patterns.extra.InputCheck;

public abstract class Tea extends Beverage {
    protected boolean isSugar;
    protected boolean isLemon;
    protected boolean isHoney;

    @Override
    public final Beverage addCondiments(Beverage beverage) {
        if(isSugar) beverage = new Sugar((Tea) beverage);
        if(isLemon) beverage = new Lemon((Tea) beverage);
        if(isHoney) beverage = new Honey((Tea) beverage);
        return beverage;
    }

    @Override
    public final int change() {
        System.out.println("1 - Sugar/Without\n2 - Lemon/Without\n3 - Honey/Without" +
                "\n4 - quantity\n0 - back");
        int i = InputCheck.get();
        switch(i) {
            case 1:
                isSugar = !isSugar;
                break;
            case 2:
                isLemon = !isLemon;
                break;
            case 3:
                isHoney = !isHoney;
                break;
            case 4:
                int num = 0;
                while (num <= 1) {
                    System.out.println("Enter quantity");
                    num = InputCheck.get();
                }
                quantity = num;
                break;
            case 0:
                break;
            default:
                System.err.println("incorrect value");
                break;
        }
        return i;
    }

    public boolean isSugar() {
        return isSugar;
    }

    public boolean isLemon() {
        return isLemon;
    }

    public boolean isHoney() {
        return isHoney;
    }

    @Override
    public String toString() {
        return description + (isSugar ? ", Sugar" : "") + (isLemon ? ", Lemon" : "") + (isHoney ? ", Honey" : "") +
                "\nquantity = " + quantity;
    }
}