package com.epam.training.patterns.entity.teas;

import com.epam.training.patterns.entity.Tea;

public class WhiteTea extends Tea {
    public WhiteTea() {
        description = "White Tea";
    }

    @Override
    public double cost() {
        return 1.60;
    }
}