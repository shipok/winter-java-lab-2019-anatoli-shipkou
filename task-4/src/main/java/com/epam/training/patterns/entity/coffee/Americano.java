package com.epam.training.patterns.entity.coffee;

import com.epam.training.patterns.entity.Coffee;

public class Americano extends Coffee {
    public Americano() {
        description = "Americano";
    }

    @Override
    public double cost() {
        return 1.59;
    }
}