package com.epam.training.patterns.entity.coffee.adding;

import com.epam.training.patterns.entity.Coffee;
import com.epam.training.patterns.entity.coffee.CoffeeCondimentDecorator;

public class Milk extends CoffeeCondimentDecorator {
    public Milk(Coffee coffee) {
        super(coffee);
    }

    @Override
    public String getDescription() {
        return coffee.getDescription() + ", Milk";
    }

    @Override
    public double cost() {
        return .10 + coffee.cost();
    }
}