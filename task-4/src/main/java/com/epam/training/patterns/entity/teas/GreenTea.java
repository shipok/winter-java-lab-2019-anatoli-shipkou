package com.epam.training.patterns.entity.teas;

import com.epam.training.patterns.entity.Tea;

public class GreenTea extends Tea {
    public GreenTea() {
        description = "Green Tea";
    }

    @Override
    public double cost() {
        return 1.27;
    }
}