package com.epam.training.patterns.entity;

public abstract class Beverage {
    protected String description = "Unknown Beverage";
    protected int quantity = 1;

    public String getDescription() {
        return description;
    }

    public final int getQuantity() {
        return quantity;
    }

    public abstract Beverage addCondiments(Beverage beverage);
    public abstract double cost();
    public abstract int change();
    public abstract String toString();
}