package com.epam.training.patterns.entity.teas;

import com.epam.training.patterns.entity.Tea;

public class BlackTea extends Tea {
    public BlackTea() {
        description = "Black Tea";
    }

    @Override
    public double cost() {
        return 1.30;
    }
}