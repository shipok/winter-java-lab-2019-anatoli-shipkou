package com.epam.training.patterns.entity.coffee.adding;

import com.epam.training.patterns.entity.Coffee;
import com.epam.training.patterns.entity.coffee.CoffeeCondimentDecorator;

public class WhippedCream extends CoffeeCondimentDecorator {
    public WhippedCream(Coffee coffee) {
        super(coffee);
    }

    @Override
    public String getDescription() {
        return coffee.getDescription() + ", Whipped Cream";
    }

    @Override
    public double cost() {
        return .10 + coffee.cost();
    }
}