package com.epam.training.collection;

import java.util.Collection;

public interface Stack<E>{
    Iterator<E> getIterator();
    boolean isEmpty();
    E peek();
    E pop();
    void push(E e);
    void pushAll(Collection<? extends E> c);
    int search(E e);
    int clear();
    int size();
    void sort(E e);
}