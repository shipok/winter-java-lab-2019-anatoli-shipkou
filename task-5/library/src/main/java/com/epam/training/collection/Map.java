package com.epam.training.collection;

public interface Map<K, V> {
    boolean isEmpty();
    void set(K key, V value);
    Entity<K, V> remote(K key);
    List<K> getKeys();
    List<V> getValues();
    V get(K key);
    Entity<K, V> getEntity(K key);
    boolean contains(V value);
    int clear();
    int size();

}