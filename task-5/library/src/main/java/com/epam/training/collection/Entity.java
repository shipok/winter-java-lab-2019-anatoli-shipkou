package com.epam.training.collection;

public interface Entity<K, V> {
    K getKey(V v);
    V getValue(K k);
}