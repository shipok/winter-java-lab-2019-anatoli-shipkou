package com.epam.training.collection;

import java.util.Collection;

public interface List<E> extends Collection<E> {
    Iterator<E> iterator();
    boolean add(E e);
    boolean addAll(Collection<? extends E> c);
    void set(int index, E element);
    E remove(int index);
    void clear();
    int find(E e);
    E get(int index);
    <T> T[] toArray(T[] a);
    int size();
    void trim();
    void filterMatches(Collection<? extends E> c);
    void filterDifference(Collection<? extends E> c);
    void setMaxSize(int index);
    int getMaxSize(int index);
}