package com.epam.training.collection;

import java.util.Collection;

public interface Queue<E> {
    Iterator<E> getIterator();
    boolean isEmpty();
    E peek();
    E pop();
    E pull();
    E remove();
    void push(E e);
    void pushAll(Collection<? extends E> c);
    int search(E e);
    int clear();
    int size();
}