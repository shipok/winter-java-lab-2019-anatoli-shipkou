package com.epam.training.collection;

public interface Iterator<E> extends java.util.Iterator<E> {
    void addBefore(E e);
    void addAfter(E e);
}