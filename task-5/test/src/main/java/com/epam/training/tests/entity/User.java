package com.epam.training.tests.entity;

import java.util.Objects;

public class User {
    private String name;
    private String email;
    private int age;
    private String sex;

    public User(String name, String email, int age, String sex) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public int getAge() {
        return age;
    }

    public String getSex() {
        return sex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return age == user.age &&
                name.equals(user.name) &&
                email.equals(user.email) &&
                sex.equals(user.sex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email, age, sex);
    }

    @Override
    public String toString() {
        return "com.epam.training.collection.entity.User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }
}