package com.epam.training.tests;

import com.epam.training.collection.ArrayList;
import com.epam.training.collection.Iterator;
import com.epam.training.tests.entity.User;

public class TestingYourLibrary {
    public static void main(String[] args) {
        ////////////////Iterator//////////////////////
        ArrayList<User> list = new ArrayList<>();

        User user0 = new User("Felix", "felix@mail.ru", 23, "male");
        User user1 = new User("Ronni", "ronni@mail.ru", 22, "male");
        User user2 = new User("Bob", "bob@mail.ru", 20, "male");
        User user3 = new User("Liza", "liza@mail.ru", 24, "female");

        list.add(user0);
        list.add(user1);
        list.add(user2);
        list.add(user3);

        Iterator<User> listIterator = list.iterator();

        while (listIterator.hasNext()) {
            User user = listIterator.next();
            System.out.println(user);
        }

        System.out.println();
        Iterator<User> listIterator2 = list.iterator();
        listIterator2.next();
        listIterator2.next();
        listIterator2.remove();

        for(User user : list) {
            System.out.println(user);
        }

        User user4 = new User("Rich", "rich@mail.ru", 44, "male");
        User user5 = new User("Rita", "rita@mail.ru", 42, "female");
        System.out.println();
        Iterator<User> listIterator3 = list.iterator();
        listIterator3.next();
        listIterator3.addBefore(user4);
        listIterator3.next();
        listIterator3.next();
        listIterator3.addAfter(user5);

        for(User user : list) {
            System.out.println(user);
        }

        ///////////////List/////////////////

    }
}