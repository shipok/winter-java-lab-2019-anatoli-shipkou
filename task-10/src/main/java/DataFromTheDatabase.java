import com.epam.training.sql.dao.DaoTheater;

public class DataFromTheDatabase {
    public static void main(String[] args) {
        DaoTheater show = new DaoTheater();
        System.out.println("Show Performances:");
        show.viewPerformances();

        String playTitle = "Michel";
        System.out.println("\nShow actors of the play \"" + playTitle + "\":");
        show.viewActorsPlay(playTitle);

        System.out.println("\nShow additional information actors:");
        show.viewInfoActors();

        System.out.println("\nDelete the phones of the actors with id=1 and id=4 ...");
        show.updatePhone(1, null);
        show.updatePhone(4, null);

        System.out.println("\nShow result:");
        show.viewInfoActors();

        System.out.println("\nNow add the phone to the actor with id=1 and update the actor with id=6 ...");
        show.updatePhone(1, "+37529-745-12-31");
        show.updatePhone(6, "+37533-512-72-74");

        System.out.println("\nShow result:");
        show.viewInfoActors();
    }
}