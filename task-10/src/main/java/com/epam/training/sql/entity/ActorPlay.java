package com.epam.training.sql.entity;

import java.util.Objects;

public class ActorPlay {
    private String actor;
    private String amplois;
    private int budget;

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getAmplois() {
        return amplois;
    }

    public void setAmplois(String amplois) {
        this.amplois = amplois;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActorPlay actorPlay = (ActorPlay) o;
        return budget == actorPlay.budget &&
                actor.equals(actorPlay.actor) &&
                amplois.equals(actorPlay.amplois);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actor, amplois, budget);
    }

    @Override
    public String toString() {
        return "Actor: " + actor + "; Amplois: " + amplois + "; Budget: " + budget;
    }
}