package com.epam.training.sql.connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLServer {
    public static Connection createConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:8083/Theater" +
                        "?verifyServerCertificate=false" +
                        "&allowPublicKeyRetrieval=true" +
                        "&useSSL=false" +
                        "&requireSSL=false" +
                        "&useLegacyDatetimeCode=false" +
                        "&amp" +
                        "&serverTimezone=UTC",
                "root", "simple");
        return connection;
    }
}