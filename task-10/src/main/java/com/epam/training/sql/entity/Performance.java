package com.epam.training.sql.entity;

import java.sql.Date;
import java.util.Objects;

public class Performance {
    private String play;
    private String director;
    private Date premiere;

    public String getPlay() {
        return play;
    }

    public void setPlay(String play) {
        this.play = play;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public Date getPremiere() {
        return premiere;
    }

    public void setPremiere(Date premiere) {
        this.premiere = premiere;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Performance that = (Performance) o;
        return play.equals(that.play) &&
                director.equals(that.director) &&
                premiere.equals(that.premiere);
    }

    @Override
    public int hashCode() {
        return Objects.hash(play, director, premiere);
    }

    @Override
    public String toString() {
        return  "Play: \"" + play + "\"; Director: " + director + "; Premiere: " + premiere;
    }
}