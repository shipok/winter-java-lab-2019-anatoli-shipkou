package com.epam.training.sql.entity;

import java.util.Objects;

public class InfoActors {
    private int ID;
    private String actor;
    private String phone;
    private String passport;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InfoActors that = (InfoActors) o;
        return ID == that.ID &&
                actor.equals(that.actor) &&
                phone.equals(that.phone) &&
                passport.equals(that.passport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, actor, phone, passport);
    }

    @Override
    public String toString() {
        return "ID: " + ID + "; actor: " + actor + "; phone: " + phone + "; passport: " + passport;
    }
}