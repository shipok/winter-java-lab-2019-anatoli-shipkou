package com.epam.training.sql.dao;

import com.epam.training.sql.connect.SQLServer;
import com.epam.training.sql.entity.ActorPlay;
import com.epam.training.sql.entity.InfoActors;
import com.epam.training.sql.entity.Performance;

import java.sql.*;

public class DaoTheater {
    public void viewPerformances() {
        String sql = "select Play_Title Play, concat(FirstName, ' ', LastName) Director, Premiere " +
                "from Performances pf " +
                "join Plays p on pf.PlayID = p.PlayID " +
                "join Directors d on  pf.DirectorID = d.DirectorID " +
                "order by Premiere;";

        try (Connection connection = SQLServer.createConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery())
        {
            while (resultSet.next()) {
                Performance performance = new Performance();
                performance.setPlay(resultSet.getString("Play"));
                performance.setDirector(resultSet.getString("Director"));
                performance.setPremiere(resultSet.getDate("Premiere"));

                System.out.println(performance);
                System.out.println("--------------------------------------------------------");
            }
        }catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void viewActorsPlay(String playTitle) {
        String sql = "select concat(FirstName, ' ', LastName) Actor, Amplois_Title Amplois, Budget, Play_Title Play " +
                "from Employment e " +
                "join Actors a on e.ActorID = a.ActorID " +
                "join Amplois al on e.AmploisID = al.AmploisID " +
                "join Performances pf on e.PerformanceID = pf.PerformanceID " +
                "join Plays p on pf.PlayID = p.PlayID " +
                "where Play_Title = ?;";

        try (Connection connection = SQLServer.createConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql))
        {
            preparedStatement.setString(1, playTitle);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ActorPlay actorPlay = new ActorPlay();
                actorPlay.setActor(resultSet.getString("Actor"));
                actorPlay.setAmplois(resultSet.getString("Amplois"));
                actorPlay.setBudget(resultSet.getInt("Budget"));
                System.out.println(actorPlay);
                System.out.println("--------------------------------------------------------");
            }
        }catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void viewInfoActors() {
        String sql = "select ia.ActorID ID,LastName Actor, Phone, Passport " +
                "from InfoActors ia " +
                "join Actors a on ia.ActorID = a.ActorID " +
                "order by LastName";

        try (Connection connection = SQLServer.createConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery())
        {
            while (resultSet.next()) {
                InfoActors infoActors = new InfoActors();
                infoActors.setID(resultSet.getInt("ID"));
                infoActors.setActor(resultSet.getString("Actor"));
                infoActors.setPhone(resultSet.getString("Phone"));
                infoActors.setPassport(resultSet.getString("Passport"));

                System.out.println(infoActors);
                System.out.println("--------------------------------------------------------");
            }
        }catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void updatePhone(int actorID, String phone) {
        String sql = "UPDATE InfoActors " +
                "SET Phone = ? " +
                "WHERE ActorID = ?;";
        try (Connection connection = SQLServer.createConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql))
        {
            preparedStatement.setString(1, phone);
            preparedStatement.setInt(2, actorID);
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}