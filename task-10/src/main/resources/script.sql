##   creating and populating a database  ##

##DROP DATABASE Theater;

CREATE DATABASE Theater;

USE Theater;
CREATE TABLE Genres (
GenreID int PRIMARY KEY AUTO_INCREMENT,
Genre_Title varchar(30) NOT NULL
);
INSERT INTO Genres (Genre_Title)
VALUES ('Vaudeville'), ('Comedy'), ('Drama'), ('Melodrama'), ('Fairy tale');

CREATE TABLE Plays (
PlayID int PRIMARY KEY AUTO_INCREMENT,
Play_Title varchar(60) NOT NULL,
Author varchar(30) NOT NULL,
GenreID int NOT NULL,
Release_Date date NOT NULL,
FOREIGN KEY(GenreID) REFERENCES Genres (GenreID)
);
INSERT INTO Plays (Play_Title, Author, GenreID, Release_Date)
VALUES ('Miller - sorcerer, trickster and matchmaker', 'Ablesimov A.O.', 1, '1779-01-20'),
('New Year of Private Gleb Sazonov', 'Abramtsev K.', 4, '2000-02-11'),
('Michel', 'Azernikov V.', 2, '2005-05-21'),
('Bamboo island', 'Bogacheva A.', 5, '1966-11-20'),
('Homecoming', 'Pinter G.', 3, '1964-09-25');

CREATE TABLE Directors (
DirectorID int PRIMARY KEY AUTO_INCREMENT,
FirstName varchar(15) NOT NULL,
Patronymic varchar(15) NOT NULL,
LastName varchar(15) NOT NULL
);
INSERT INTO Directors (FirstName, Patronymic, LastName)
VALUES ('Yuri', 'Nikolaevich', 'Zenkov'),
('Gleb', 'Olegovich', 'Tarasov'),
('Stanislav', 'Sergeevich', 'Paraev'),
('Sergey', 'Sergeevich', 'Lapenko');

CREATE TABLE Performances (
PerformanceID int PRIMARY KEY AUTO_INCREMENT,
PlayID int NOT NULL,
DirectorID int NOT NULL,
Premiere date NOT NULL,
FOREIGN KEY(PlayID) REFERENCES Plays (PlayID),
FOREIGN KEY(DirectorID) REFERENCES Directors (DirectorID)
);
INSERT INTO Performances (PlayID, DirectorID, Premiere)
VALUES (3, 1, '2020-01-17'),
(2, 2, '2020-02-08'),
(5, 4, '2020-02-22'),
(4, 3, '2020-03-07'),
(1, 1, '2020-03-14');

CREATE TABLE Ranks (
RankID int PRIMARY KEY AUTO_INCREMENT,
Rank_Title varchar(45) NOT NULL
);
INSERT INTO Ranks (Rank_Title)
VALUES ('People\'s Artist of Belarus'), ('Honored Artist of the Republic of Belarus'), ('Artist');

CREATE TABLE Actors (
ActorID int PRIMARY KEY AUTO_INCREMENT,
FirstName varchar(25) NOT NULL,
Patronymic varchar(25) NOT NULL,
LastName varchar(25) NOT NULL,
Sex varchar(6) NOT NULL,
Birthday date NOT NULL,
RankID int NOT NULL,
FOREIGN KEY(RankID) REFERENCES Ranks (RankID)
);

CREATE TABLE InfoActors (
ActorID int PRIMARY KEY,
Phone varchar(16) NULL,
Passport varchar(9) NULL,
Education varchar (60) NULL,
FOREIGN KEY(ActorID) REFERENCES Actors (ActorID)
);

CREATE TRIGGER InfoActors_INSERT
AFTER INSERT
ON Actors
FOR EACH ROW
INSERT INTO InfoActors(ActorID) VALUES (NEW.ActorID);

INSERT INTO Actors (FirstName, Patronymic, LastName, Sex, Birthday, RankID)
VALUES ('Alla', 'Mikhailovna', 'Lenaya', 'female', '1980-12-24', 1),
('Yuri', 'Semenovich', 'Martinovich', 'male', '1969-08-09', 1),
('Tatiana', 'Nikolaevna', 'Kondratenko', 'female', '1985-03-15', 2),
('Pavel', 'Vladimirovich', 'Kordik', 'male', '1981-06-24', 1),
('Andrey', 'Petrovich', 'Shidlovsky', 'male', '1985-04-20', 2),
('Dmitry', 'Valerevich', 'Baykov', 'male', '1983-05-13', 2),
('Mikhail', 'Alexsandrovich', 'Grishechkin', 'male', '1990-10-16', 3),
('Irina', 'Vladimirovna', 'Kublitscaya', 'female', '1993-11-09', 3);

UPDATE InfoActors
SET Phone = '+37544-152-44-69', Passport = 'HB9362545', Education = 'graduated from Belarusian State University of Culture' WHERE ActorID = 1;
UPDATE InfoActors
SET Phone = '+37544-152-39-69', Passport = 'HB3657845', Education = NULL WHERE ActorID = 2;
UPDATE InfoActors
SET Phone = '+37533-639-45-26', Passport = 'HB6364579', Education = 'graduated from the Belarusian Academy of Arts' WHERE ActorID = 3;
UPDATE InfoActors
SET Phone = '+37544-323-98-69', Passport = 'HB3695696', Education = 'graduated from the Belarusian State Academy of Arts' WHERE ActorID = 4;
UPDATE InfoActors
SET Phone = '+37529-456-82-13', Passport = 'HB1245869', Education = 'graduated from the Mogilev College of Arts' WHERE ActorID = 5;
UPDATE InfoActors
SET Phone = '+37544-156-48-26', Passport = 'HB6598656', Education = 'graduate of the Belarusian Academy of Arts' WHERE ActorID = 6;
UPDATE InfoActors
SET Phone = '+37529-693-57-53', Passport = 'HB3457898', Education = NULL WHERE ActorID = 7;
UPDATE InfoActors
SET Phone = '+37533-932-45-12', Passport = 'HB5683626', Education = 'graduate of the Mogilev Theater College' WHERE ActorID = 8;

CREATE TABLE Amplois (
AmploisID int PRIMARY KEY AUTO_INCREMENT,
Amplois_Title varchar(50) NOT NULL
);
INSERT INTO Amplois (Amplois_Title)
VALUES ('Girl'), ('Young man'), 
('Magician'), ('Soldier'), ('Student'), ('Passersby'), ('Man on duty'), ('Chief'),
('Man'), ('Woman'),
('Peasant'), ('Groom'), ('Miller'),
('Mother'), ('Turtle'), ('Monkey'), ('Dragon'), ('Rabbit');

CREATE TABLE Employment (
EmploymentID int PRIMARY KEY AUTO_INCREMENT,
ActorID int NOT NULL,
AmploisID int NOT NULL,
PerformanceID int NOT NULL,
Budget int NOT NULL,
FOREIGN KEY(ActorID) REFERENCES Actors (ActorID),
FOREIGN KEY(AmploisID) REFERENCES Amplois (AmploisID),
FOREIGN KEY(PerformanceID) REFERENCES Performances (PerformanceID)
);
INSERT INTO Employment (ActorID, AmploisID, PerformanceID, Budget)
VALUES (3, 1, 1, 500), (8, 1, 1, 500), (6, 2, 1, 620), (7, 2, 1, 340),
(2, 3, 2, 320), (7, 4, 2, 530), (8, 5, 2, 450), (3, 1, 2, 450), (5, 5, 2, 400), (6, 6, 2, 200), (1, 7, 2, 510), (4, 8, 2, 310),
(2, 9, 3, 320), (4, 9, 3, 320), (5, 9, 3, 450), (6, 9, 3, 220), (7, 9, 3, 100), (3, 10, 3, 640),
(8, 1, 5, 400), (7, 12, 5, 420), (1, 10, 5, 300), (4, 11, 5, 390), (5, 13, 5, 570),
(7, 5, 4, 210), (5, 5, 4, 210), (3, 14, 4, 250), (1, 15, 4, 150), (8, 16, 4, 290), (2, 17, 4, 100), (6, 18, 4, 180);