package com.epam.training.reflection;

import com.epam.training.reflection.entity.ServicesImpl;
import com.epam.training.reflection.entity.MetaService;
import com.epam.training.reflection.entity.TimeService;

import java.lang.reflect.Proxy;

public class ProxyTest {
    public static void main(String[] args) {
        ServicesImpl metaService = new ServicesImpl();

        MetaService metaServiceProxy = (MetaService) Proxy.newProxyInstance(
                metaService.getClass().getClassLoader(),
                metaService.getClass().getInterfaces(),
                new HandlerMetaService(metaService));

        System.out.println("Method: printMetadataInfo");
        metaServiceProxy.printMetadataInfo();
        System.out.println("\nMethod: printMetadataExpiredTime");
        metaServiceProxy.printMetadataExpiredTime();
        System.out.println("\nMethod: printMetadataUser");
        metaServiceProxy.printMetadataUser();
        System.out.println("\nMethod: printMetadataBuilder");
        metaServiceProxy.printMetadataBuilder();

        TimeService timeServiceProxy = (TimeService) Proxy.newProxyInstance(
                metaService.getClass().getClassLoader(),
                metaService.getClass().getInterfaces(),
                new HandlerMetaService(metaService));

        System.out.println("\nMethod: printTimeInfo");
        timeServiceProxy.printTimeInfo();
        System.out.println("\nMethod: printTimeExpiredTime");
        timeServiceProxy.printTimeExpiredTime();
        System.out.println("\nMethod: printTimeUser");
        timeServiceProxy.printTimeUser();
        System.out.println("\nMethod: printTimeBuilder");
        timeServiceProxy.printTimeBuilder();
    }
}