package com.epam.training.reflection.exception;

public class NotSupportedMethodException extends Exception {
    @Override
    public String toString() {
        return "Exception is thrown -- NotSupportedMethodException";
    }
}