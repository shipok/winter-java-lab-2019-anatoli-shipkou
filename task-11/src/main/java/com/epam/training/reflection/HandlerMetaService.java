package com.epam.training.reflection;

import com.epam.training.reflection.annotation.*;
import com.epam.training.reflection.entity.ServicesImpl;
import com.epam.training.reflection.exception.NotSupportedMethodException;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class HandlerMetaService implements InvocationHandler {
    private ServicesImpl servicesImpl;

    public HandlerMetaService(ServicesImpl servicesImpl) {
        this.servicesImpl = servicesImpl;
    }

    private Method getClassMethod(Method methodInterface) throws NoSuchMethodException {
        return servicesImpl.getClass().getMethod(methodInterface.getName());
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] objects) throws Throwable {
        if (servicesImpl.getClass().isAnnotationPresent(Ignore.class)) {
            method.invoke(servicesImpl);
            return null;
        }

        if (servicesImpl.getClass().isAnnotationPresent(Before.class)) {
            Before before = servicesImpl.getClass().getAnnotation(Before.class);
            System.out.println(before.message());
        }

        if (getClassMethod(method).isAnnotationPresent(Ignore.class)) {
            method.invoke(servicesImpl);
            if (servicesImpl.getClass().isAnnotationPresent(After.class)) {
                After before = servicesImpl.getClass().getAnnotation(After.class);
                System.out.println(before.message());
            }
            return null;
        }

        if (getClassMethod(method).isAnnotationPresent(ThrowException.class)) {
            try {
                throw new NotSupportedMethodException();
            } catch (NotSupportedMethodException e) {
                System.out.println(e);
                if (servicesImpl.getClass().isAnnotationPresent(After.class)) {
                    After before = servicesImpl.getClass().getAnnotation(After.class);
                    System.out.println(before.message());
                }
                return null;
            }
        }

        Annotation[] annotations = getClassMethod(method).getDeclaredAnnotations();
        for (Annotation a : annotations) {
            if (a instanceof RepeatA) {
                method.invoke(servicesImpl);
                RepeatA anno = getClassMethod(method).getAnnotation(RepeatA.class);
                After[] annos = anno.value();
                for (After after : annos) {
                    System.out.println(after.message());
                }
                if (servicesImpl.getClass().isAnnotationPresent(After.class)) {
                    After before = servicesImpl.getClass().getAnnotation(After.class);
                    System.out.println(before.message());
                }
                return null;
            }
            if (a instanceof RepeatB) {
                RepeatB anno = getClassMethod(method).getAnnotation(RepeatB.class);
                Before[] annos = anno.value();
                for (Before before : annos) {
                    System.out.println(before.message());
                }
                method.invoke(servicesImpl);
                if (servicesImpl.getClass().isAnnotationPresent(After.class)) {
                    After before = servicesImpl.getClass().getAnnotation(After.class);
                    System.out.println(before.message());
                }
                return null;
            }
        }

        if (getClassMethod(method).isAnnotationPresent(Before.class)) {
            Before before = getClassMethod(method).getAnnotation(Before.class);
            System.out.println(before.message());
        }

        method.invoke(servicesImpl);

        if (getClassMethod(method).isAnnotationPresent(After.class)) {
            After after = getClassMethod(method).getAnnotation(After.class);
            System.out.println(after.message());
        }

        if (servicesImpl.getClass().isAnnotationPresent(After.class)) {
            After before = servicesImpl.getClass().getAnnotation(After.class);
            System.out.println(before.message());
        }
        return null;
    }
}