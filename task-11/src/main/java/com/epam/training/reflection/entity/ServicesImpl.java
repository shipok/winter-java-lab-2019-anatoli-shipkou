package com.epam.training.reflection.entity;

import com.epam.training.reflection.annotation.*;

@Before(message = "Class annotation -- @Before")
public class ServicesImpl implements MetaService, TimeService {
    @After(message = "Annotation message: @After info 1")
    @After(message = "Annotation message: @After info 2")
    public void printMetadataInfo() {
        System.out.println("Original message: 'Metadata Info'");
    }

    @ThrowException
    public void printMetadataExpiredTime() {
        System.out.println("Original message: 'Metadata Expired Time'");
    }

    @Before(message = "Annotation message: @Before User")
    public void printMetadataUser() {
        System.out.println("Original message: 'Metadata User'");
    }

    @Before(message = "Annotation message: @Before Builder 1")
    @Before(message = "Annotation message: @Before Builder 2")
    public void printMetadataBuilder() {
        System.out.println("Original message: 'Metadata Builder'");
    }

    @Before(message = "Annotation message: @Before Info")
    public void printTimeInfo() {
        System.out.println("Original message: 'Time Info'");
    }

    public void printTimeExpiredTime() {
        System.out.println("Original message: 'Time Expired Time'");
    }

    @After(message = "Annotation message: @After User")
    public void printTimeUser() {
        System.out.println("Original message: 'Time User'");
    }

    @Ignore
    public void printTimeBuilder() {
        System.out.println("Original message: 'Time Builder'");
    }
}