package com.epam.training.reflection.entity;

public interface MetaService {
    void printMetadataInfo();
    void printMetadataExpiredTime();
    void printMetadataUser();
    void printMetadataBuilder();
}