package com.epam.training.reflection.entity;

public interface TimeService {
    void printTimeInfo();
    void printTimeExpiredTime();
    void printTimeUser();
    void printTimeBuilder();
}