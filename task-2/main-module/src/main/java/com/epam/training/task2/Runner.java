package com.epam.training.task2;

import com.epam.training.task2.date.ShowDate;
import com.epam.training.task2.format.FormatDate;

public class Runner {
    public static void main(String[] args) {
        FormatDate formatDate = new FormatDate("(dd) (MMMM) (ss) (hh) (yyyy) (mm)");

        System.out.println("Hello world: " + formatDate.newView(ShowDate.showCurrentDate()));
        System.out.print("Hello world + 1 week: " + formatDate.newView(ShowDate.showInWeekDate()));
    }
}
