package com.epam.training.task2.format;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FormatDate {
    private String pattern;

    public FormatDate(String pattern){
        this.pattern = pattern;
    }

    public String newView(LocalDateTime date) {
        return date.format(DateTimeFormatter.ofPattern(pattern));
    }
}
