package com.epam.training.task2.date;

import java.time.LocalDateTime;

public class ShowDate {
    public static LocalDateTime showCurrentDate() {
        return LocalDateTime.now();
    }

    public static LocalDateTime showInWeekDate() {
        return LocalDateTime.now().plusWeeks(1);
    }
}