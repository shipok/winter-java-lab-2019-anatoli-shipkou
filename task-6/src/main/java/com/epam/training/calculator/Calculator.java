package com.epam.training.calculator;

import com.epam.training.calculator.entity.Example;
import com.epam.training.calculator.metods.Handler;

import java.io.IOException;
import java.util.ArrayList;

public class Calculator {
    public static void main(String[] args) throws IOException {
        ArrayList<Example> examples = Handler.getExamples(args[0]);
        examples = Handler.decision(examples);

        if (!examples.isEmpty()) {
            Handler.selectExample(examples);
        } else {
            System.out.print("File was empty");
        }
    }
}