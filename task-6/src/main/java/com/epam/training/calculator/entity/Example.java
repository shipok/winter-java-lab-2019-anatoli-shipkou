package com.epam.training.calculator.entity;

import java.util.LinkedList;

public class Example {
    LinkedList<StringBuilder> exampleSteps = new LinkedList<>();;

    public LinkedList<StringBuilder> getExampleSteps() {
        return exampleSteps;
    }

    public void addExampleSteps(StringBuilder example) {
        exampleSteps.add(example);
    }

    public void setExampleSteps(LinkedList<StringBuilder> exampleSteps) {
        this.exampleSteps = exampleSteps;
    }

    public int getSteps() {
        return exampleSteps.size() - 2;
    }
}