package com.epam.training.calculator.metods;

import com.epam.training.calculator.entity.Example;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class Handler {
    public static ArrayList<Example> getExamples(String path) throws IOException {
        ArrayList<Example> examples = new ArrayList<>();
        StringBuilder step0 = new StringBuilder();

        FileReader fr = new FileReader(path);
        int c;
        while ((c = fr.read()) != -1) {
            if (c != 32 && c != 13 && c != 10) {
                step0.append((char) c);
            }
            if (c == 13 && step0.length() != 0) {
                Example example = new Example();
                example.addExampleSteps(step0);
                examples.add(example);
                step0 = new StringBuilder();
            }
        }
        if (step0.length() != 0) {
            Example example = new Example();
            example.addExampleSteps(step0);
            examples.add(example);
        }
        return examples;
    }

    public static ArrayList<Example> decision(ArrayList<Example> examples) {
        ArrayList<Example> examplesWithSteps = new ArrayList<>();
        for (Example e : examples) {
            e = eval(e);
            examplesWithSteps.add(e);
        }
        return examplesWithSteps;
    }

    static boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    static int priority(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }

    static StringBuilder processOperator(LinkedList<Double> st, char op) {
        double r = st.removeLast();
        double l = st.removeLast();
        switch (op) {
            case '+':
                st.add(l + r);
                return new StringBuilder(l + "+" + r);
            case '-':
                st.add(l - r);
                return new StringBuilder(l + "-" + r);
            case '*':
                st.add(l * r);
                return new StringBuilder(l + "*" + r);
            case '/':
                st.add(l / r);
                return new StringBuilder(l + "/" + r);
            default:
                return new StringBuilder("");
        }

    }

    public static Example eval(Example example) {
        LinkedList<StringBuilder> steps = example.getExampleSteps();
        String step0 = steps.get(0).toString();

        try {
            LinkedList<Double> st = new LinkedList<>(); // insert numbers here
            LinkedList<Character> op = new LinkedList<>(); // operators, st and op in the order of arrival
            for (int i = 0; i < step0.length(); i++) {
                char c = step0.charAt(i);
                if (c == '(')
                    op.add('(');
                else if (c == ')') {
                    while (op.getLast() != '(')
                        steps.add(processOperator(st, op.removeLast()));
                    op.removeLast();
                } else if (isOperator(c)) {
                    while (!op.isEmpty() && priority(op.getLast()) >= priority(c))
                        steps.add(processOperator(st, op.removeLast()));
                    op.add(c);
                } else {
                    String operand = "";
                    while (i < step0.length() && (Character.isDigit(step0.charAt(i)) || step0.charAt(i) == '.'))
                        operand += step0.charAt(i++);
                    --i;
                    st.add(Double.parseDouble(operand));
                }
            }

            while (!op.isEmpty()) steps.add(processOperator(st, op.removeLast()));
            steps.add(new StringBuilder(st.get(0) + ""));
            example.setExampleSteps(steps);
            return example;
        } catch (RuntimeException e) {
            Example invalid = new Example();
            invalid.addExampleSteps(new StringBuilder("Incorrect expression"));
            return invalid;
        }
    }

    public static void selectExample(ArrayList<Example> examples) {
        System.out.println("Result:");
        int count = 1;
        for (Example e : examples) {
            if (e.getExampleSteps().size() == 1) {
                System.out.println(count++ + ". " + e.getExampleSteps().get(0));
            } else {
                System.out.println(count++ + ". " + e.getSteps() + " steps; " + e.getExampleSteps().getLast() +
                        " result; " + e.getExampleSteps().getFirst());
            }
        }


        System.out.println("\nSelect example and step");
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();
        String[] result = str.split("\\s+");
        int check = -1;
        try {
            int exam = Integer.parseInt(result[0]);
            Example example = examples.get(exam - 1);
            check = 0;

            int step = Integer.parseInt(result[1]);
            if (step > 0) step++;
            System.out.println("Result:\n" + exam + ". " + example.getExampleSteps().get(step));
        } catch (NumberFormatException e) {
            System.err.println("Non-integer values were entered");
        } catch (IndexOutOfBoundsException e) {
            if (check ==0) {
                System.out.println("Result:\n" + result[0] + ". " + "Incorrect value");
            } else {
                System.out.println("Result:\nexample " + result[0] + ". " + "Does not exist");
            }
        }
    }
}