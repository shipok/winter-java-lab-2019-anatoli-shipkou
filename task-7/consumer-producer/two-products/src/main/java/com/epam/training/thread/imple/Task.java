package com.epam.training.thread.imple;

public class Task {
    private String task;
    private int product;

    public void setTask(String task) {
        this.task = task;
    }

    public String getTask() {
        return task;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public int action() {
        return product;
    }

    public void display() {
        System.out.println("Display: " + product);
    }
}