package com.epam.training.thread.workers;

import com.epam.training.thread.imple.Task;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {
    static final int ACTION = 0;
    static final int DISPLAY = 1;

    private BlockingQueue<Task> queue;

    public Producer(BlockingQueue<Task> queue) {
        this.queue = queue;
        new Thread(this, "Producer").start();
    }

    @Override
    public void run() {
        Task task = new Task();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int r = random.nextInt(2);
            if (r == ACTION) task.setTask("Action");
            if (r == DISPLAY) task.setTask("Display");
            task.setProduct(i);

            try {
                queue.put(task);
                System.out.println("Sent: " + i + " for " + task.getTask());
                Thread.sleep(10);    //for clarity
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
    }
}