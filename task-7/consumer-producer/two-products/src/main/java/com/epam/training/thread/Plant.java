package com.epam.training.thread;

import com.epam.training.thread.imple.Task;
import com.epam.training.thread.workers.Consumer;
import com.epam.training.thread.workers.Producer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Plant {
    public static void main(String[] args) {
        BlockingQueue<Task> queue = new ArrayBlockingQueue<>(1);

        new Producer(queue);
        new Consumer(queue);
    }
}