package com.epam.training.thread.workers;

import com.epam.training.thread.imple.Task;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {
    static final String ACTION = "Action";
    static final String DISPLAY = "Display";

    private BlockingQueue<Task> queue;

    public Consumer(BlockingQueue<Task> queue) {
        this.queue = queue;
        new Thread(this, "Consumer").start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                Task task = queue.take();

                if (task.getTask().equals(ACTION)) System.out.println("Return: " + task.action());
                if (task.getTask().equals(DISPLAY)) task.display();

            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
    }
}