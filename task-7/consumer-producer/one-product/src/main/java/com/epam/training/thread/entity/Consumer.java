package com.epam.training.thread.entity;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Consumer implements Runnable {
    private BlockingQueue<Integer> queue;
    private int consumer;

    public Consumer(BlockingQueue<Integer> queue, int consumer) {
        this.queue = queue;
        this.consumer = consumer;
        new Thread(this, "Consumer").start();
    }

    @Override
    public void run() {
        for (int i = 0; i < consumer; i++) {
            try {
                System.out.println("Received: " + queue.poll(100L, TimeUnit.MILLISECONDS));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}