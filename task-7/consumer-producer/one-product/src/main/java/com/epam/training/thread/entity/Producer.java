package com.epam.training.thread.entity;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Producer implements Runnable {
    private BlockingQueue<Integer> queue;
    private int producer;

    public Producer(BlockingQueue<Integer> queue, int producer) {
        this.queue = queue;
        this.producer = producer;
        new Thread(this, "Producer").start();
    }

    @Override
    public void run() {
        int n = 0;
        for (int i = 0; i < producer; i++) {
            try {
                queue.offer(++n, 100L, TimeUnit.MILLISECONDS);
                System.out.println("Sent: " + n);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
    }
}