package com.epam.training.thread;

import com.epam.training.thread.entity.Consumer;
import com.epam.training.thread.entity.Producer;
import com.epam.training.thread.extra.InputCheck;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Factory {
    public static void main(String[] args) {
        int producer, consumer;

        System.out.println("Enter quantity for producer");
        producer = InputCheck.get();

        System.out.println("Enter quantity for consumer");
        consumer = InputCheck.get();

        BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(1);

        new Producer(queue, producer);
        new Consumer(queue, consumer);
    }
}