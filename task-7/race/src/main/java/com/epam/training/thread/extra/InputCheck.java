package com.epam.training.thread.extra;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputCheck {
    public static int get() {
        int i = -1;

        int e = -1;
        while (e != 0) {
            Scanner scan = new Scanner(System.in);
            try {
                i = scan.nextInt();
                if (i < 1) System.err.println("Enter from one and above");
                else e = 0;
            } catch (InputMismatchException u) {
                System.err.println("Enter only numbers");
            }
        }
        return i;
    }
}