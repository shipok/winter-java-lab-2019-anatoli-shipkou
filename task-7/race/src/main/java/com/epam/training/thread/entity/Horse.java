package com.epam.training.thread.entity;

import com.epam.training.thread.extra.Speed;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class Horse implements Runnable {
    private CyclicBarrier start;
    private double lengthTrack;
    private int number;
    private CountDownLatch finish;
    private List<Integer> result;

    private static final Logger LOGGER = LogManager.getLogger("Horse");

    public Horse(CyclicBarrier start, int lengthTrack, int number, CountDownLatch finish, List<Integer> result) {
        this.start = start;
        this.lengthTrack = lengthTrack;
        this.number = number;
        this.finish = finish;
        this.result = result;
        new Thread(this, "" + number).start();
    }

    @Override
    public void run() {
        int time = 0;
        double speedHorse = Speed.ask();

        try {
            start.await();
            LOGGER.info("-- " + lengthTrack + " m --> start");

            while (lengthTrack >= 0) {
                Thread.sleep(1);
                loggerTime(lengthTrack, ++time);
                lengthTrack -= speedHorse;
            }
            result.add(number);

            finish.countDown();
        } catch (BrokenBarrierException | InterruptedException ex) {
            System.out.println(ex);
        }
    }

    private static void loggerTime(double lengthTrack, int time) {
        if(time % 1000 == 0) LOGGER.info("--> " + lengthTrack);
    }
}