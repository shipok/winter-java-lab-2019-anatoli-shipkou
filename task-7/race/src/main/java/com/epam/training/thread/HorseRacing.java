package com.epam.training.thread;

import com.epam.training.thread.entity.Horse;
import com.epam.training.thread.extra.InputCheck;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class HorseRacing {
    public static  void main(String[] args) {
        System.out.println("Enter the quantity of horses");
        int quantityHorse = InputCheck.get();

        System.out.println("Enter the length of the track in meters");
        int lengthTrack = InputCheck.get();

        CyclicBarrier start = new CyclicBarrier(quantityHorse);
        CountDownLatch finish = new CountDownLatch(quantityHorse);

        List<Integer> result = new ArrayList<>();

        for (int i = 1; i <= quantityHorse; i++) {
            new Horse(start, lengthTrack, i, finish, result);
        }

        try {
            finish.await();
        } catch (InterruptedException e) {
            System.out.println(e);
        }

        System.out.println("\nThe race is over, WINNER horse at number " + result.get(0) + " !!!");
        System.out.println("Result of other riders:");
        for (int i = 2; i <= result.size(); i++) {
            System.out.println(i + " -- horse at number: " + result.get(i - 1));
        }
    }
}