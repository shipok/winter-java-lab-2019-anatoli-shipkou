package com.epam.training.thread.extra;

public class Speed {
    public static double ask() {
        int min = 153;
        int max = 166;
        max -= min;
        return ((Math.random() * ++max) + min) / 10000;
    }
}