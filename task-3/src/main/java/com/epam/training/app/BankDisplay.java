package com.epam.training.app;

import com.epam.training.dao.Display;
import com.epam.training.dao.SelectionTransaction;
import com.epam.training.dao.SettingFromFile;
import com.epam.training.dao.UpdateDatabase;
import com.epam.training.model.Settings;
import com.epam.training.wrapp.SettingsBank;
import com.epam.training.model.Transaction;

import java.io.IOException;
import java.util.List;

public class BankDisplay {
    private static final String directory = "data";
    public static void main(String[] args) throws IOException {
        SettingsBank settingsBank = new SettingFromFile().getFromFile(directory);
        Settings settings = settingsBank.getSettings();

        List<Transaction> selectBranch = new SelectionTransaction().collect(directory, settings.getUseDepartments());

        new UpdateDatabase().update(selectBranch, directory);

        new Display(settings);
    }
}