package com.epam.training.wrapp;

import com.epam.training.model.Transaction;

import java.util.LinkedList;
import java.util.List;

public class BranchBank {
    private List<Transaction> transactions = new LinkedList<>();

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void addTransactions(Transaction transaction) {
        transactions.add(transaction);
    }
}