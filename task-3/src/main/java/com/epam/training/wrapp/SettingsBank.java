package com.epam.training.wrapp;

import com.epam.training.model.Settings;

public class SettingsBank {
    private Settings settings;

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }
}