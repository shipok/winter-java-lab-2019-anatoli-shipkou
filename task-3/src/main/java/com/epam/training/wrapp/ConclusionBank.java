package com.epam.training.wrapp;

import com.epam.training.model.Conclusion;

import java.util.LinkedList;
import java.util.List;

public class ConclusionBank {
    List<Conclusion> conclusions = new LinkedList<>();

    public List<Conclusion> getConclusions() {
        return conclusions;
    }

    public void setConclusions(List<Conclusion> conclusions) {
        this.conclusions = conclusions;
    }

    public void addConclusion(Conclusion conclusion) {
        conclusions.add(conclusion);
    }
}