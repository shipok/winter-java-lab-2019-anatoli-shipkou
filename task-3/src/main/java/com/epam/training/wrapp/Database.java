package com.epam.training.wrapp;

import com.epam.training.model.Credit;
import com.epam.training.model.Transaction;
import com.epam.training.model.User;

import java.util.LinkedList;
import java.util.List;

public class Database {
    private List<User> users = new LinkedList<>();
    private List<Credit> credits = new LinkedList<>();
    private List<Transaction> transactions = new LinkedList<>();

    public List<User> getUsers() {
        return new LinkedList<User>(users);
    }

    public void addUsers(User users) {
        this.users.add(users);
    }

    public List<Credit> getCredits() {
        return new LinkedList<Credit>(credits);
    }

    public void addCredits(Credit credits) {
        this.credits.add(credits);
    }

    public List<Transaction> getTransactions() {
        return new LinkedList<Transaction>(transactions);
    }

    public void addTransactions(Transaction transactions) {
        this.transactions.add(transactions);
    }
}