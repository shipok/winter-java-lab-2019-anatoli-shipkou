package com.epam.training.dao;

import com.epam.training.wrapp.SettingsBank;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class SettingFromFile {
    private SettingsBank settings;

    public SettingsBank getFromFile(String directory) throws IOException {
        File fileSettings = new File(directory, "settings.json");
        if (fileSettings.exists()) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            settings = gson.fromJson(new FileReader(fileSettings), SettingsBank.class);
        } else {
            System.err.println("The file \"settings.json\" is not in the directory");
        }
        return settings;
    }
}