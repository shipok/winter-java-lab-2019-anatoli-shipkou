package com.epam.training.dao;

import com.epam.training.extra.DatePeriod;
import com.epam.training.extra.Sorting;
import com.epam.training.model.*;
import com.epam.training.wrapp.ConclusionBank;
import com.epam.training.wrapp.Database;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class Display {
    public Display(Settings settings) throws FileNotFoundException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        ConclusionBank conclusionBank = new ConclusionBank();

        Database database = gson.fromJson(new FileReader("data/db.json"), Database.class);
        List<Credit> credits = database.getCredits();
        List<User> users = database.getUsers();
        List<Transaction> transactions = database.getTransactions();

        ShowFor showFor = settings.getShowFor();
        int [] userId = showFor.getUsers();
        for(int id:userId) {
            Optional<Credit> opc = credits.stream().filter(a -> a.getUserId() == id).findAny();
            Credit credit = opc.get();

            Optional<User> opu = users.stream().filter(a -> a.getId() == id).findAny();
            User u = opu.get();
            String fullName = u.getName() + " " + u.getSecondName();

            Conclusion conclusion = new Conclusion(credit.getId(), id, fullName, credit.getPeriod());
            conclusionBank.addConclusion(conclusion);
        }

        List<Transaction> newTrans = transactions.stream().filter(a ->
                LocalDate.parse(settings.getDateFrom()).compareTo(LocalDate.parse(a.getDate())) <= 0 &&
                LocalDate.parse(settings.getDateTo()).compareTo(LocalDate.parse(a.getDate())) >= 0).collect(Collectors.toList());

        List<Conclusion> newConclusions = new LinkedList<>();

        for(Conclusion c : conclusionBank.getConclusions()) {
            long amountTrans = newTrans.stream().filter(a -> a.getCreditId() == c.getCreditId()).count();
            c.setAmountTransaction(amountTrans);

            Optional<Credit> opc = credits.stream().filter(a -> a.getId() == c.getCreditId()).findAny();
            Credit credit = opc.get();

            String accrualDate = DatePeriod.get(credit.getDate(), credit.getPeriod());
            double money = credit.getMoney();
            double rate = credit.getRate()/100 + 1;

            DoubleStream stream = newTrans.stream().filter(a -> a.getCreditId() == c.getCreditId())
                    .filter(a -> LocalDate.parse(a.getDate()).compareTo(LocalDate.parse(accrualDate)) <= 0)
                    .mapToDouble(a -> a.getMoney());
            double sumPaidBefore = stream.sum();
            money = money - sumPaidBefore;

            if(LocalDate.parse(accrualDate).compareTo(LocalDate.now()) < 0
            && LocalDate.parse(accrualDate).compareTo(LocalDate.parse(settings.getDateTo())) <= 0) {
                money = money * rate;
            }

            stream = newTrans.stream().filter(a -> a.getCreditId() == c.getCreditId())
                    .filter(a -> LocalDate.parse(a.getDate()).compareTo(LocalDate.parse(accrualDate)) > 0)
                    .mapToDouble(a -> a.getMoney());
            double sumPaidAfter = stream.sum();
            double balance = money - sumPaidAfter;
            c.setDebtAmount(balance);

            String status;
            if(balance <= 0){
                status = "DONE";
            }else {
                status = "IN PROGRESS";
            }
            c.setStatus(status);

            newConclusions.add(c);
        }

        conclusionBank.setConclusions(Sorting.sort(newConclusions, settings.getSortBy()));
        String display = gson.toJson(conclusionBank);
        System.out.println(display);
    }
}