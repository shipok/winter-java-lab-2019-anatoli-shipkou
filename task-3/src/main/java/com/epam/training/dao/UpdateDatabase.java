package com.epam.training.dao;

import com.epam.training.wrapp.Database;
import com.epam.training.model.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

//class to update the database
public class UpdateDatabase {
    private Database database;

    public Database update(List<Transaction> transactions, String directory) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        File fileDb = new File(directory,"db.json");

        if(fileDb.exists()) {
            database = gson.fromJson(new FileReader(fileDb), Database.class);
            transactions.forEach(a -> database.addTransactions(a));
            String json = gson.toJson(database);
            FileWriter fw = new FileWriter(fileDb.getAbsoluteFile());
            fw.write(json);
            fw.close();
            System.out.println("Transactions of all branches are added to the database");
        }else {
            System.err.println("The file \"db.json\" is not in the directory");
        }
        return database;
    }
}