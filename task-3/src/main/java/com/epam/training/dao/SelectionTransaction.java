package com.epam.training.dao;

import com.epam.training.wrapp.BranchBank;
import com.epam.training.model.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class SelectionTransaction {
    private List<Transaction> allBranch = new LinkedList<>();

    public List<Transaction> collect(String directory, String[] useDepartments) throws IOException {
        for (String s : useDepartments) {
            File file = new File(directory, "db_" + s + ".json");
            if(file.exists()) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                BranchBank branch = gson.fromJson(new FileReader(file), BranchBank.class);
                List<Transaction> list = branch.getTransactions();

                BranchBank empty = new BranchBank();
                String e = gson.toJson(empty);
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                fw.write(e);
                fw.close();

                allBranch.addAll(list);
            }else {
                System.out.println("The file \"" + file.getName() + "\" is not found");
            }
        }
        return allBranch;
    }
}