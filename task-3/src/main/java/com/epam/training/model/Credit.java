package com.epam.training.model;

public class Credit {
    private int id;
    private int userId;
    private String date;
    private String period;
    private double money;
    private double rate;

    public Credit() {
    }

    public Credit(int id, int userId, String date, String period, double money, double rate) {
        this.id = id;
        this.userId = userId;
        this.date = date;
        this.period = period;
        this.money = money;
        this.rate = rate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}