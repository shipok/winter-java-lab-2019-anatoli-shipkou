package com.epam.training.model;

public class Transaction {
    private int id;
    private String date;
    private int creditId;
    private double money;

    public Transaction() {
    }

    public Transaction(int id, String date, int creditId, double money) {
        this.id = id;
        this.date = date;
        this.creditId = creditId;
        this.money = money;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getCreditId() {
        return creditId;
    }

    public void setCreditId(int creditId) {
        this.creditId = creditId;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}