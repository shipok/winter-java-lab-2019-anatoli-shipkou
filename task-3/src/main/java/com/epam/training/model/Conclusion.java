package com.epam.training.model;

public class Conclusion {
    private int creditId;
    private int userId;
    private String fullName;
    private long amountTransaction;
    private double debtAmount;
    private String period;
    private String status;

    public Conclusion() {
    }

    public Conclusion(int creditId, int userId, String fullName, String period) {
        this.creditId = creditId;
        this.userId = userId;
        this.period = period;
        this.fullName = fullName;
    }

    public int getCreditId() {
        return creditId;
    }

    public void setCreditId(int creditId) {
        this.creditId = creditId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public long getAmountTransaction() {
        return amountTransaction;
    }

    public void setAmountTransaction(long amountTransaction) {
        this.amountTransaction = amountTransaction;
    }

    public double getDebtAmount() {
        return debtAmount;
    }

    public void setDebtAmount(double debtAmount) {
        this.debtAmount = debtAmount;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}