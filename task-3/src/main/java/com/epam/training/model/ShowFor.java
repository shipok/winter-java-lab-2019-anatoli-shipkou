package com.epam.training.model;

public class ShowFor {
    private String type;
    private int [] users;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int[] getUsers() {
        return users;
    }

    public void setUsers(int[] users) {
        this.users = users;
    }
}