package com.epam.training.model;

public class Settings {

    private String dateFrom;
    private String dateTo;
    private ShowFor showFor;
    private String sortBy;
    private String[] useDepartments;

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public ShowFor getShowFor() {
        return showFor;
    }

    public void setShowFor(ShowFor showFor) {
        this.showFor = showFor;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String[] getUseDepartments() {
        return useDepartments;
    }

    public void setUseDepartments(String[] useDepartments) {
        this.useDepartments = useDepartments;
    }
}