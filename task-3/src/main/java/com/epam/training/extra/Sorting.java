package com.epam.training.extra;

import com.epam.training.model.Conclusion;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Sorting {
    public static List<Conclusion> sort(List<Conclusion> conclusions, String sortBy) {
        switch (sortBy) {
            case "NAME" :
                System.out.println("Sort by client name");
                return conclusions.stream().sorted(Comparator.comparing(Conclusion::getFullName)).collect(Collectors.toList());
            case "DEBT" :
                System.out.println("Sort by debt");
                return conclusions.stream().sorted(Comparator.comparing(Conclusion::getDebtAmount)).collect(Collectors.toList());
            default:
                return conclusions;
        }
    }
}