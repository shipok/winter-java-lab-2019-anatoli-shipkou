package com.epam.training.extra;

import java.time.LocalDate;

public class DatePeriod {
    public static String get (String date, String period) {
        String newDate = "";
        switch (period) {
            case "DAY" :
                newDate = LocalDate.parse(date).plusDays(1).toString();
                break;
            case "WEEK" :
                newDate = LocalDate.parse(date).plusWeeks(1).toString();
                break;
            case "MONTH" :
                newDate = LocalDate.parse(date).plusMonths(1).toString();
                break;
            case "YEAR" :
                newDate = LocalDate.parse(date).plusYears(1).toString();
                break;
        }
        return newDate;
    }
}