package com.epam.training.xml.additions;

import com.epam.training.xml.entity.Person;

import java.util.ArrayList;

public class Changes {
    public static void newWorkByName(ArrayList<Person> persons, String firstLetter) {
        for (Person person : persons) {
            if (person.getName().startsWith(firstLetter)) {
                person.setWork(selectWork());
            }
        }
    }

    private static String selectWork() {
        String[] works = {"TOP", "NEWS", "SYSTEM", "OWN"};
        int n = (int) Math.floor(Math.random() * works.length);
        return works[n];
    }
}