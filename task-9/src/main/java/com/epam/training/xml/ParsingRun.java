package com.epam.training.xml;

import com.epam.training.xml.additions.Changes;
import com.epam.training.xml.entity.Person;
import com.epam.training.xml.parse.Parser;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import java.io.FileReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class ParsingRun {
    private static ArrayList<Person> persons;

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        DocumentBuilderFactory dbp = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbp.newDocumentBuilder();
        Document doc = db.parse(new File("converter/people.xml"));

        NodeList personElements = doc.getElementsByTagName("person");
        persons = Parser.fromXML(personElements);

        persons.add(2, new Person(persons.size() + 1, "Pavlov", "Pavel", "23.02.1989",
                "Mogilev", "EPAM"));

        Changes.newWorkByName(persons, "I");

        Parser.toXMLFile("converter/people-modified.xml", persons);

        doc = db.parse(new File("converter/people-modified.xml"));
        personElements = doc.getElementsByTagName("person");
        persons = Parser.fromXML(personElements);

        Collections.sort(persons);
        Parser.toXMLFile("converter/people-result.xml", persons);

        FileReader readXML = new FileReader(new File("converter/people-result.xml"));
        int c;
        while((c = readXML.read())!= -1) System.out.print((char)c);
    }
}