package com.epam.training.xml.parse;

import com.epam.training.xml.entity.Person;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Parser {
    public static ArrayList<Person> fromXML(NodeList personElements) {
        ArrayList<Person> persons = new ArrayList<>();

        for (int i = 0; i < personElements.getLength(); i++) {
            Node person = personElements.item(i);

            if (person.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) person;
                String date = "";
                date += element.getElementsByTagName("birthday").item(0).getChildNodes().item(1).getTextContent();
                date += "." + element.getElementsByTagName("birthday").item(0).getChildNodes().item(3).getTextContent();
                date += "." + element.getElementsByTagName("birthday").item(0).getChildNodes().item(5).getTextContent();

                persons.add(new Person(Integer.parseInt(element.getAttribute("ID")),
                        element.getElementsByTagName("surname").item(0).getTextContent(),
                        element.getElementsByTagName("name").item(0).getTextContent(),
                        date,
                        element.getElementsByTagName("birthplace").item(0).getAttributes().getNamedItem("city").getNodeValue(),
                        element.getElementsByTagName("work").item(0).getTextContent()));

            }
        }
        return persons;
    }

    public static void toXMLFile(String pathToSave, ArrayList<Person> persons) throws ParserConfigurationException,
            TransformerException, IOException {

        DocumentBuilderFactory dbp = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbp.newDocumentBuilder();
        Document newDoc = db.newDocument();

        Element root = newDoc.createElement("people");

        for (Person exp : persons) {
            String[] date = exp.getBirthday().split("\\.");

            Element person = newDoc.createElement("person");
            person.setAttribute("ID", String.valueOf(exp.getId()));

            Element surname = newDoc.createElement("surname");
            surname.setTextContent(exp.getSurname());

            Element name = newDoc.createElement("name");
            name.setTextContent(exp.getName());

            Element birthday = newDoc.createElement("birthday");
            Element day = newDoc.createElement("day");
            day.setTextContent(date[0]);
            Element month = newDoc.createElement("month");
            month.setTextContent(date[1]);
            Element year = newDoc.createElement("year");
            year.setTextContent(date[2]);
            birthday.appendChild(day);
            birthday.appendChild(month);
            birthday.appendChild(year);

            Element birthplace = newDoc.createElement("birthplace");
            birthplace.setAttribute("city", exp.getBirthplace());

            Element work = newDoc.createElement("work");
            work.setTextContent(exp.getWork());

            person.appendChild(surname);
            person.appendChild(name);
            person.appendChild(birthday);
            person.appendChild(birthplace);
            person.appendChild(work);

            root.appendChild(person);
        }

        newDoc.appendChild(root);
        DOMSource source = new DOMSource(newDoc);

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        FileWriter fw = new FileWriter(new File(pathToSave));
        StreamResult result = new StreamResult(fw);
        transformer.transform(source, result);
    }
}