package com.epam.training.xml.entity;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Person implements Comparable<Person> {
    private int id;
    private String surname;
    private String name;
    private String birthday;
    private String birthplace;
    private String work;

    public Person(int id, String surname, String name, String birthday, String birthplace, String work) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.birthday = birthday;
        this.birthplace = birthplace;
        this.work = work;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    @Override
    public int compareTo(Person person) {
        return LocalDate.parse(this.getBirthday(), DateTimeFormatter.ofPattern("d.MM.yyyy"))
                .compareTo(LocalDate.parse(person.getBirthday(), DateTimeFormatter.ofPattern("d.MM.yyyy")));
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", birthday='" + birthday + '\'' +
                ", birthplace='" + birthplace + '\'' +
                ", work='" + work + '\'' +
                '}';
    }
}